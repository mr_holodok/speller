//
// Created by mukola on 02.03.20.
//

#ifndef SPELLER_HASHMAPCHECKER_H
#define SPELLER_HASHMAPCHECKER_H

#include <unordered_map>
#include "checker.h"

class HashmapChecker : public AbstractChecker {
public:
    HashmapChecker() : map{std::unordered_map<size_t, std::string>()}, hash_func{std::hash<std::string>()} {};

    ~HashmapChecker() = default;

    void add(const std::string &s) override;

    bool check(const std::string &s) const override;

    std::string get_name() const override {
        return "HashMap";
    }

private:
    std::unordered_map<size_t, std::string> map;
    std::hash<std::string> hash_func;
};


#endif //SPELLER_HASHMAPCHECKER_H
