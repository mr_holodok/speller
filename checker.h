//
// Created by mukola on 01.03.20.
//

#ifndef SPELLER_CHECKER_H
#define SPELLER_CHECKER_H

#include <string>

class AbstractChecker {
public:
    virtual ~AbstractChecker() {};

    virtual void add(const std::string &s) = 0;

    virtual bool check(const std::string &s) const = 0;

    virtual std::string get_name() const = 0;
};

#endif //SPELLER_CHECKER_H