//
// Created by mukola on 01.03.20.
//

#ifndef SPELLER_VECTOR_CHECKER_H
#define SPELLER_VECTOR_CHECKER_H

#include "checker.h"
#include <vector>

class VectorChecker : public AbstractChecker {
public:
    VectorChecker() : vector{std::vector<std::string>()} {};

    ~VectorChecker() = default;

    void add(const std::string &s) override;

    bool check(const std::string &s) const override;

    std::string get_name() const override {
        return "Vector";
    }

private:
    std::vector<std::string> vector;
};


#endif //SPELLER_VECTOR_CHECKER_H
