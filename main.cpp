#include <iostream>
#include <chrono>
#include <boost/filesystem.hpp>
#include <set>

#include "checker.h"
#include "vector_checker.h"
#include "bin_tree_checker.h"
#include "hash_table_checker.h"
#include "hashmap_checker.h"
#include "utilities.h"

namespace fs = boost::filesystem;

namespace FilesAndDirsNames {
    const std::string path_to_texts_dir{fs::current_path().string() + "/texts"};
    const std::string path_to_wrong_words_dir{fs::current_path().string() + "/check_results"};
    const std::string path_to_vocabulary{fs::current_path().string() + "/dictionary.txt"};
}

int main() {
    std::ifstream dict_file(FilesAndDirsNames::path_to_vocabulary);
    std::vector<std::string> words;

    // reading words to vector
    if (!dict_file) {
        std::cout << "Error reading file dictionary.txt" << std::endl;
        return 1;
    }

    std::string word;
    while (dict_file >> word) {
        words.emplace_back(word);
    }
    dict_file.close();

    std::vector<std::chrono::milliseconds> load_times;
    std::vector<std::chrono::milliseconds> check_times;
    std::vector<uint> checked_words_counts;
    std::vector<uint> wrong_words_counts;

    std::vector<std::shared_ptr<AbstractChecker>> checkers = {
            std::make_unique<HashTableChecker>(),
            std::make_unique<VectorChecker>(),
            std::make_unique<HashmapChecker>(),
            std::make_unique<BinTreeChecker>(),
    };

    for (const auto &checker : checkers) {

        auto start = std::chrono::high_resolution_clock::now();
        for (const auto &word : words) {
            checker->add(word);
        }
        auto end = std::chrono::high_resolution_clock::now();

        load_times.emplace_back(std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count());
#ifdef LOG_ENABLED
        std::cout << checker->get_name() << " DICTIONARY LOADED [OK]" << std::endl;
#endif
    }

    // we no longer need 'words' now, so free them
    words.clear();
    words.shrink_to_fit();

    std::vector<fs::path> texts_filenames;

    // get names of the files with texts from folder  
    fs::path texts_path{FilesAndDirsNames::path_to_texts_dir};
    if (fs::exists(texts_path) && fs::is_directory(texts_path)) {
        for (auto &&entry : fs::directory_iterator(texts_path)) {
            if (fs::is_regular_file(entry.path()) && entry.path().extension() == ".txt")
                texts_filenames.push_back(entry.path());
        }
    } else {
        std::cout << "Can\'t find files with texts! Please check, that folder texts exists!" << std::endl;
        return 1;
    }

    // ensure that folder for results is exists
    if (!fs::exists(FilesAndDirsNames::path_to_wrong_words_dir)) {
        fs::create_directory(FilesAndDirsNames::path_to_wrong_words_dir);
    }

    // generating files with wrong (unknown) words step
    // std::set to save words without duplicates
    std::set<std::string> wrong_words{};
    for (const auto &path : texts_filenames) {
#ifdef LOG_ENABLED
        std::cout << "EXTRACTING WORDS from " << path.filename().string() << std::endl;
#endif
        words = extract_lowercase_words(path.string());
#ifdef LOG_ENABLED
        std::cout << "WORDS EXTRACTED [OK]" << std::endl;
#endif

        // checking words ONLY BY MEANS OF ONE OF CHECKERS
        for (const auto &word : words) {
            if (!checkers[0]->check(word)) {
                wrong_words.insert(word);
            }
        }
        // writing words to files associated by name with checked file
        std::string filename = FilesAndDirsNames::path_to_wrong_words_dir + "/" + path.stem().string()
                               + "_checked" + path.extension().string();
        std::ofstream result_file(filename);
        if (!result_file) {
            std::cout << "Some troubles with creating result files! Please try again!" << std::endl;
            return 1;
        }
        for (const auto &word : wrong_words) {
            result_file << word << std::endl;
        }
#ifdef LOG_ENABLED
        std::cout << "UNKNOWN WORDS SAVED [OK] to " << path.filename().stem().string() << "_checked" << path.extension().string() << std::endl;
#endif
    }

    // measuring step
    // PAY ATTENTION: here map is not used, so count of wrong words in
    // these results may differ from words count in files generated in previous step
    for (const auto &checker : checkers) {

        auto summary_duration = std::chrono::milliseconds();
        uint checked_words_count = 0, wrong_words_count = 0;

        for (const auto &path : texts_filenames) {
            words = extract_lowercase_words(path.string());
            auto start = std::chrono::high_resolution_clock::now();
            for (const auto &word : words) {
                ++checked_words_count;
                if (!checker->check(word)) {
                    ++wrong_words_count;
                }
            }
            auto end = std::chrono::high_resolution_clock::now();
            summary_duration += std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
#ifdef LOG_ENABLED
            std::cout << "FILE " << path.filename().string() << " CHECKED [OK]" << std::endl;
#endif
        }

#ifdef LOG_ENABLED
        std::cout << "CHECKER " << checker->get_name() << " MEASURED [OK]" << std::endl;
#endif

        check_times.push_back(summary_duration);
        checked_words_counts.push_back(checked_words_count);
        wrong_words_counts.push_back(wrong_words_count);
    }


    uint i = 0;
    for (const auto &checker : checkers) {
        std::cout << checker->get_name() << "\t" << std::to_string(load_times[i].count()) << "\t" <<
                  std::to_string(check_times[i].count()) << "\t" <<
                  std::to_string(checked_words_counts[i]) << "\t" <<
                  std::to_string(wrong_words_counts[i]) << std::endl;
        ++i;
    }

    return 0;
}
