//
// Created by mukola on 02.03.20.
//
// AVL BINARY TREE is preferred (to ordinary BST), because
// in worst case BST creates list of nodes (as our dictionary
// is sorted we will get right-branched tree)

#ifndef SPELLER_BINTREECHECKER_H
#define SPELLER_BINTREECHECKER_H

#include <memory>
#include "checker.h"

class BinTreeChecker : public AbstractChecker {

    struct BinTreeNode {
    public:
        std::shared_ptr<BinTreeNode> left, right;
        std::string value;
        int height;

        explicit BinTreeNode(const std::string &v) : left(), right(), value{v}, height(1) {};
    };

public:
    BinTreeChecker() : root{nullptr} {};

    ~BinTreeChecker() = default;

    void add(const std::string &s) override;

    bool check(const std::string &s) const override;

    std::string get_name() const override {
        return "BinaryTree";
    }

private:
    std::shared_ptr<BinTreeNode> root;

    std::shared_ptr<BinTreeNode> add(const std::string &s, std::shared_ptr<BinTreeNode> node);

    int height(std::shared_ptr<BinTreeNode> node) const;

    int get_balance_factor(std::shared_ptr<BinTreeNode> node) const;

    std::shared_ptr<BinTreeNode> left_rotate(std::shared_ptr<BinTreeNode> node);

    std::shared_ptr<BinTreeNode> right_rotate(std::shared_ptr<BinTreeNode> node);
};

#endif //SPELLER_BINTREECHECKER_H
