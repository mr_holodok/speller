//
// Created by mukola on 03.03.20.
//

#ifndef SPELLER_UTILITIES_H
#define SPELLER_UTILITIES_H

#include <vector>
#include <string>
#include <fstream>
#include <memory>
#include <algorithm>


// especially for substrings
std::string to_lower(std::string &&str) {
    std::for_each(str.begin(), str.end(), [](char &c) {
        c = ::tolower(c);
    });
    return str;
}

std::vector<std::string> extract_lowercase_words(const std::string &filename) {
    enum ParseState {
        START, STRING
    };
    std::vector<std::string> words;
    std::ifstream file(filename);

    if (!file) {
        throw std::invalid_argument("Can\'t open file!");
    }

    std::string line;
    ParseState state = ParseState::START;
    while (file.good()) {
        std::getline(file, line);
        uint word_start = 0, i = 0;
        while (i < line.size()) {
            switch (state) {
                case ParseState::START :
                    // ASSUME THAT WORD STARTS WITH LETTER
                    if (isalpha(line[i])) {
                        word_start = i;
                        state = ParseState::STRING;
                    } else {
                        ++i;
                    }
                    break;
                case ParseState::STRING :
                    if (!isalpha(line[i]) && line[i] != '\'') {
                        // extra check for <'> at the end of the word as words in
                        // vocabulary doesn't contain words with <'> at the end
                        // hint : reg exp - ([a-z])*'\n
                        if (!isalpha(line[i - 1])) {
                            words.emplace_back(to_lower(line.substr(word_start, i - word_start - 1)));
                        } else {
                            words.emplace_back(to_lower(line.substr(word_start, i - word_start)));
                        }
                        state = ParseState::START;
                        ++i;
                    } else if (i == line.size() - 1) {
                        // last letter, so add word
                        words.emplace_back(to_lower(line.substr(word_start, i - word_start + 1)));
                        state = ParseState::START;
                        ++i; // to quit from loop
                    } else {
                        // not last char and current char is valid, so just keep going
                        ++i;
                    }
                    break;
            }
        }
    }

    return words;
}

#endif //SPELLER_UTILITIES_H