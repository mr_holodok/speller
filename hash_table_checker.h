//
// Created by mukola on 03.03.20.
//

#ifndef SPELLER_HASHTABLECHECKER_H
#define SPELLER_HASHTABLECHECKER_H

#include "checker.h"
#include <vector>

class HashTableChecker : public AbstractChecker {
public:
    HashTableChecker() : table{std::vector<std::string>(1000)} {};

    explicit HashTableChecker(uint n) : table{std::vector<std::string>(n)} {};

    ~HashTableChecker() = default;

    void add(const std::string &s) override;

    bool check(const std::string &s) const override;

    std::string get_name() const override {
        return "HashTable";
    }

    float get_load_factor() const;

    uint get_max_collisions_count() const;

private:
    std::vector<std::string> table;
    uint count = 0;
    uint max_collisions_count = 0;

    uint32_t get_hash(const std::string &s) const;

    void extend_table();
};


#endif //SPELLER_HASHTABLECHECKER_H