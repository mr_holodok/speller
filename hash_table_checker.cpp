//
// Created by mukola on 03.03.20.
//

#include "hash_table_checker.h"

namespace HashConstants {
    const int p = 21;
}

void HashTableChecker::add(const std::string &s) {
    uint32_t index = get_hash(s);

    uint init_index = index;
    while (index < table.size() && !table[index].empty()) {
        ++index;
    }

    if (index == table.size()) {
        extend_table();
        add(s);
    } else if (table[index].empty()) {
        if (get_load_factor() > 0.8) {
            extend_table();
        }
        table[index] = s;
        ++count;
        if (index - init_index > max_collisions_count) {
            max_collisions_count = index - init_index;
        }
    }
}

bool HashTableChecker::check(const std::string &s) const {
    uint32_t index = get_hash(s);

    while (index < table.size() && !table[index].empty() && table[index] != s) {
        ++index;
    }

    return !(index == table.size() || table[index].empty());
}

uint32_t HashTableChecker::get_hash(const std::string &s) const {
    uint32_t hash = 0;
    uint32_t p_pow = 1;
    for (char c : s) {
        hash += c * p_pow;
        p_pow *= HashConstants::p;
    }
    return hash % table.size();
}

float HashTableChecker::get_load_factor() const {
    return ((float) count) / table.size();
}

void HashTableChecker::extend_table() {
    std::vector<std::string> old_table{std::move(table)};
    table = std::vector<std::string>(old_table.size() * 2);

    for (const auto &cell : old_table) {
        if (cell.empty()) {
            continue;
        }
        add(cell);
    }
}

uint HashTableChecker::get_max_collisions_count() const {
    return max_collisions_count;
}