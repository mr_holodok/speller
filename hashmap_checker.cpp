//
// Created by mukola on 02.03.20.
//

#include "hashmap_checker.h"

void HashmapChecker::add(const std::string &s) {
    auto key = hash_func(s);
    if (map.find(key) != map.end()) {
        throw std::runtime_error("Key already exist!");
    }
    map[key] = s;
}

bool HashmapChecker::check(const std::string &s) const {
    auto key = hash_func(s);
    return !(map.find(key) == map.end());
}