//
// Created by mukola on 01.03.20.
//

#include "vector_checker.h"
#include <algorithm>

void VectorChecker::add(const std::string &s) {
    vector.push_back(s);
}

bool VectorChecker::check(const std::string &s) const {
    auto result = std::find(vector.begin(), vector.end(), s);
    if (result == vector.end())
        return false;
    return true;
}