# Speller

Program represents benchmark of 4 data structures: `hash table`, `std::vector`, `std::unordered_map`, `binary tree`.
Benchmark compares time of `loading words`, `checking words`, counts all `checked words` and all `wrong words` 
(words that not present in data structure, so unknown).

Vocabulary with words for loading data structures is located in `dictionary.txt`, 
texts for checking data structures are at `texts` folder.

Sample output:

```$xslt
HashTable  305	8     29759 281
Vector	   77	65101 29759 281
HashMap	   282  12    29759 281
BinaryTree 1960	122   29759 281
```

# notes
**Boost::filesystem required!**

Use this [link](https://www.boost.org/doc/libs/1_72_0/more/getting_started/unix-variants.html) for installing Boost on UNIX.
Or this [one](https://www.boost.org/doc/libs/1_72_0/more/getting_started/windows.html) for Windows.

*AND DON'T FORGET to set **BOOST_ROOT** environment variable!*
