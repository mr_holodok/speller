//
// Created by mukola on 02.03.20.
//

#include "bin_tree_checker.h"

void BinTreeChecker::add(const std::string &s) {
    root = add(s, root);
}

std::shared_ptr<BinTreeChecker::BinTreeNode>
BinTreeChecker::add(const std::string &s, std::shared_ptr<BinTreeNode> node) {
    if (node) {
        if (s < node->value) {
            node->left = add(s, node->left);
        } else if (s > node->value) {
            node->right = add(s, node->right);
        } else {
            return node;
        }
    } else {
        node = std::make_shared<BinTreeNode>(s);
        return node;
    }

    // update height (also recursively)
    node->height = 1 + std::max(height(node->left), height(node->right));
    int balance = get_balance_factor(node);
    // Left Left Case
    if (balance > 1 && s < node->left->value) {
        return right_rotate(node);
    }
    // Right Right Case  
    if (balance < -1 && s > node->right->value) {
        return left_rotate(node);
    }
    // Left Right Case  
    if (balance > 1 && s > node->left->value) {
        node->left = left_rotate(node->left);
        return right_rotate(node);
    }
    // Right Left Case  
    if (balance < -1 && s < node->right->value) {
        node->right = right_rotate(node->right);
        return left_rotate(node);
    }

    return node;
}

int BinTreeChecker::height(std::shared_ptr<BinTreeNode> node) const {
    if (node) {
        return node->height;
    }
    return 0;
}

int BinTreeChecker::get_balance_factor(std::shared_ptr<BinTreeNode> node) const {
    if (node) {
        return height(node->left) - height(node->right);
    }
    return 0;
}

std::shared_ptr<BinTreeChecker::BinTreeNode> BinTreeChecker::left_rotate(std::shared_ptr<BinTreeNode> node) {
    /*
     *       A (node)             С (return node)
     *     /   \                /   \
     *    B     C       ==>    A     E
     *         /  \          /   \
     *        D    E        B     D
     */

    auto c = node->right;
    auto d = c->left;

    c->left = node;
    node->right = d;

    node->height = 1 + std::max(height(node->left), height(node->right));
    c->height = 1 + std::max(height(c->left), height(c->right));
    return c;
}

std::shared_ptr<BinTreeChecker::BinTreeNode> BinTreeChecker::right_rotate(std::shared_ptr<BinTreeNode> node) {
    /*
     *       A (node)             B (return node)
     *     /   \                /   \
     *    B     C       ==>    D     A
     *  /   \                      /   \
     * D     E                    E     C
     */

    auto b = node->left;
    auto e = b->right;

    b->right = node;
    node->left = e;

    node->height = 1 + std::max(height(node->left), height(node->right));
    b->height = 1 + std::max(height(b->left), height(b->right));

    return b;
}

bool BinTreeChecker::check(const std::string &s) const {
    std::shared_ptr<BinTreeNode> current(root);

    while (current) {
        if (current->value == s)
            return true;
        else if (s > current->value && current->right)
            current = current->right;
        else if (s < current->value && current->left)
            current = current->left;
        else
            current = nullptr;
    }

    return false;
}